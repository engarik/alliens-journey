package com.engarik.aliensjourney.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.engarik.aliensjourney.AliensJourney;
import com.engarik.aliensjourney.Utils.Values;

public class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = Values.TITLE + " " + Values.VERSION;
        config.width = 1280;
        config.height = 720;
        config.fullscreen = false;
        new LwjglApplication(AliensJourney.getInstance(), config);
    }
}
