package com.engarik.aliensjourney.Handlers;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.engarik.aliensjourney.AliensJourney;
import com.engarik.aliensjourney.Utils.Values;

public class LevelHandler extends ButtonHandler{

    private int lvl;

    public LevelHandler(int level) {
        this.lvl = level;
    }

    public void clicked(InputEvent event, float x, float y) {
        super.clicked(event, x, y);
        Values.current_lvl = lvl;
        AliensJourney.getInstance().setScreen("PlayScreen");

    }
}
