package com.engarik.aliensjourney.Handlers;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.engarik.aliensjourney.AliensJourney;

public class ScreensHandler extends ButtonHandler{
    private String name;

    public ScreensHandler(String name) {
        this.name = name;
    }

    public void clicked(InputEvent event, float x, float y) {
        super.clicked(event, x, y);
        AliensJourney.getInstance().setScreen(name);
    }
}
