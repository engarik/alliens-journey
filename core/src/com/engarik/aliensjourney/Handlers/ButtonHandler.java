package com.engarik.aliensjourney.Handlers;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.engarik.aliensjourney.AliensJourney;
import com.engarik.aliensjourney.Utils.Values;

public class ButtonHandler extends ClickListener{

    private Sound sound;

    public ButtonHandler() {
        sound = AliensJourney.getInstance().getManager().get("toLoad/sounds/clickSnd.ogg");
    }


    public void clicked(InputEvent event, float x, float y) {
        sound.play(Values.FX_SOUND);
    }
}
