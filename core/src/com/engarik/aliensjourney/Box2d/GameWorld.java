package com.engarik.aliensjourney.Box2d;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.engarik.aliensjourney.AliensJourney;
import com.engarik.aliensjourney.Box2d.Handlers.MyContactListener;
import com.engarik.aliensjourney.Box2d.Sprites.Player;
import com.engarik.aliensjourney.Utils.Values;

public class GameWorld {

    private TiledMap map;
    private OrthogonalTiledMapRenderer mapRenderer;

    // Box2d

    private World world;
    private Box2DDebugRenderer b2dRenderer;
    private OrthographicCamera b2dCamera;

    private Player player;

    public GameWorld() {

        String mapName = "toLoad/levels/level" + Values.current_lvl + ".tmx";

        map = AliensJourney.getInstance().getManager().get(mapName);
        mapRenderer = new OrthogonalTiledMapRenderer(map);

        world = new World(new Vector2(0, -9.81f), true);
        world.setContactListener(new MyContactListener());
        b2dRenderer = new Box2DDebugRenderer();

        b2dCamera = new OrthographicCamera();
        b2dCamera.setToOrtho(false, Values.WIDTH / B2dValues.PPM, Values.HEIGHT / B2dValues.PPM);

        player = new Player(world, getPlayerStatingPos());


        createBodies();
    }


    public void render() {
        mapRenderer.render();
        b2dRenderer.render(world, b2dCamera.combined);
    }

    public void update(OrthographicCamera camera) {
        world.step(B2dValues.TIME_STEP, B2dValues.VEL_ITERATIONS, B2dValues.POS_ITERATIONS);
        mapRenderer.setView(camera);
        b2dCamera.update();
    }

    private Vector2 getPlayerStatingPos() {
        MapObject object = map.getLayers().get(1).getObjects().getByType(RectangleMapObject.class).first();
        Rectangle rect = ((RectangleMapObject) object).getRectangle();
        return new Vector2(rect.getX(), rect.getY());
    }

    private void createBodies() {
        BodyDef bodyDef = new BodyDef();
        PolygonShape shape = new PolygonShape();
        FixtureDef fixtureDef = new FixtureDef();
        Body body;
        // Creating invisible walls
        for (MapObject object : map.getLayers().get(2).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();

            bodyDef.type = BodyDef.BodyType.StaticBody;
            bodyDef.position.set((rect.getX() + rect.getWidth() / 2) / B2dValues.PPM, (rect.getY() + rect.getHeight() / 2) / B2dValues.PPM);
            body = world.createBody(bodyDef);

            shape.setAsBox(rect.getWidth() / 2 / B2dValues.PPM, rect.getHeight() / 2 / B2dValues.PPM);
            fixtureDef.shape = shape;
            fixtureDef.filter.categoryBits = B2dValues.BIT_GROUND;
            fixtureDef.filter.maskBits = B2dValues.BIT_PLAYER;
            Fixture fixture = body.createFixture(fixtureDef);
            fixture.setUserData("ground");

        }
        // Creating ground tiles
        for (MapObject object : map.getLayers().get(3).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();

            bodyDef.type = BodyDef.BodyType.StaticBody;
            bodyDef.position.set((rect.getX() + rect.getWidth() / 2) / B2dValues.PPM, (rect.getY() + rect.getHeight() / 2) / B2dValues.PPM);
            body = world.createBody(bodyDef);

            shape.setAsBox(rect.getWidth() / 2 / B2dValues.PPM, rect.getHeight() / 2 / B2dValues.PPM);
            fixtureDef.shape = shape;
            fixtureDef.filter.categoryBits = B2dValues.BIT_GROUND;
            fixtureDef.filter.maskBits = B2dValues.BIT_PLAYER;
            Fixture fixture = body.createFixture(fixtureDef);
            fixture.setUserData("ground");

        }
        // Creating spikes tiles
        for (MapObject object : map.getLayers().get(4).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();

            bodyDef.type = BodyDef.BodyType.StaticBody;
            bodyDef.position.set((rect.getX() + rect.getWidth() / 2) / B2dValues.PPM, (rect.getY() + rect.getHeight() / 2) / B2dValues.PPM);
            body = world.createBody(bodyDef);

            shape.setAsBox(rect.getWidth() / 2 / B2dValues.PPM, rect.getHeight() / 2 / B2dValues.PPM);
            fixtureDef.shape = shape;
            fixtureDef.filter.categoryBits = B2dValues.BIT_SPIKES;
            fixtureDef.filter.maskBits = B2dValues.BIT_PLAYER;
            Fixture fixture = body.createFixture(fixtureDef);
            fixture.setUserData("spikes");

        }
        // Creating silver coins
        for (MapObject object : map.getLayers().get(5).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();

            bodyDef.type = BodyDef.BodyType.StaticBody;
            bodyDef.position.set((rect.getX() + rect.getWidth() / 2) / B2dValues.PPM, (rect.getY() + rect.getHeight() / 2) / B2dValues.PPM);
            body = world.createBody(bodyDef);

            shape.setAsBox(rect.getWidth() / 2 / B2dValues.PPM, rect.getHeight() / 2 / B2dValues.PPM);
            fixtureDef.shape = shape;
            fixtureDef.filter.categoryBits = B2dValues.BIT_COIN_SILVER;
            fixtureDef.filter.maskBits = B2dValues.BIT_PLAYER;
            Fixture fixture = body.createFixture(fixtureDef);
            fixture.setUserData("silverCoins");

        }

        // Creating bronze coins
        for (MapObject object : map.getLayers().get(6).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();

            bodyDef.type = BodyDef.BodyType.StaticBody;
            bodyDef.position.set((rect.getX() + rect.getWidth() / 2) / B2dValues.PPM, (rect.getY() + rect.getHeight() / 2) / B2dValues.PPM);
            body = world.createBody(bodyDef);

            shape.setAsBox(rect.getWidth() / 2 / B2dValues.PPM, rect.getHeight() / 2 / B2dValues.PPM);
            fixtureDef.shape = shape;
            fixtureDef.filter.categoryBits = B2dValues.BIT_COIN_BRONZE;
            fixtureDef.filter.maskBits = B2dValues.BIT_PLAYER;
            Fixture fixture = body.createFixture(fixtureDef);
            fixture.setUserData("bronzeCoins");

        }

        // Creating finnish body
        for (MapObject object : map.getLayers().get(7).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();

            bodyDef.type = BodyDef.BodyType.StaticBody;
            bodyDef.position.set((rect.getX() + rect.getWidth() / 2) / B2dValues.PPM, (rect.getY() + rect.getHeight() / 2) / B2dValues.PPM);
            body = world.createBody(bodyDef);

            shape.setAsBox(rect.getWidth() / 2 / B2dValues.PPM, rect.getHeight() / 2 / B2dValues.PPM);
            fixtureDef.shape = shape;
            fixtureDef.filter.categoryBits = B2dValues.BIT_FINISH;
            fixtureDef.filter.maskBits = B2dValues.BIT_PLAYER;
            Fixture fixture = body.createFixture(fixtureDef);
            fixture.setUserData("finnish");

        }
    }

    public void handleInput(OrthographicCamera camera, float delta) {
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            if (camera.position.x - 350 * delta > 960) {
                camera.position.x -= 350 * delta;
                b2dCamera.position.x -= (350 * delta) / B2dValues.PPM;
            } else {
                camera.position.x = 960;
                b2dCamera.position.x = 960 / B2dValues.PPM;
            }
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT) && camera.position.x < 2240) {
            if (camera.position.x + 350 * delta < 2240) {
                camera.position.x += 350 * delta;
                b2dCamera.position.x += (350 * delta) / B2dValues.PPM;
            } else {
                camera.position.x = 2240;
                b2dCamera.position.x = 2240 / B2dValues.PPM;
            }
        }
        if (Gdx.input.isKeyPressed(Input.Keys.SPACE) | (Gdx.input.isTouched() && Gdx.input.getY() < 540) && player.b2body.getPosition().y * B2dValues.PPM < 1080) {
            player.b2body.applyForceToCenter(0, 25, true);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.A) || (Gdx.input.isTouched() && Gdx.input.getX() < 960 && Gdx.input.getY() > 540) ) {
            player.b2body.applyForceToCenter(-25, 0, true);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.D) || (Gdx.input.isTouched() && Gdx.input.getX() > 960 && Gdx.input.getY() > 540)) {
            player.b2body.applyForceToCenter(25, 0, true);
        }

    }

    public void dispose(){
        map.dispose();
        mapRenderer.dispose();
        world.dispose();
        b2dRenderer.dispose();
        player.dispose();
    }

    public void trace(OrthographicCamera camera, float delta) {
        float posX = 0, posY = 540;

        if(player.b2body.getPosition().x * B2dValues.PPM > 960 && player.b2body.getPosition().x * B2dValues.PPM < 2240)
            posX = player.b2body.getPosition().x * B2dValues.PPM;
        else if(player.b2body.getPosition().x * B2dValues.PPM < 960)
            posX = 960;
        else if(player.b2body.getPosition().x * B2dValues.PPM > 2240)
            posX = 2240;

        camera.position.set(posX, posY, 0);
        b2dCamera.position.set(posX / B2dValues.PPM, posY / B2dValues.PPM, 0);
    }
}
