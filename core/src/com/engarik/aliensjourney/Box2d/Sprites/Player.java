package com.engarik.aliensjourney.Box2d.Sprites;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.engarik.aliensjourney.Box2d.B2dValues;

public class Player extends Sprite{

    public World world;
    public Body b2body;

    public Player(World world, Vector2 startPos){
        this.world = world;
        defPlayer(startPos.x, startPos.y);
    }

    private void defPlayer(float x, float y){
        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(x / B2dValues.PPM,y / B2dValues.PPM);
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        b2body = world.createBody(bodyDef);

        FixtureDef fixtureDef = new FixtureDef();
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(60 / B2dValues.PPM,98 / B2dValues.PPM);

        fixtureDef.shape = shape;
        fixtureDef.filter.categoryBits = B2dValues.BIT_PLAYER;
        fixtureDef.filter.maskBits =
                                B2dValues.BIT_GROUND | B2dValues.BIT_SPIKES |
                                B2dValues.BIT_COIN_BRONZE | B2dValues.BIT_COIN_SILVER |
                                B2dValues.BIT_COIN_GOLD | B2dValues.BIT_FINISH;
        Fixture fixture = b2body.createFixture(fixtureDef);
        fixture.setUserData("player");
    }

    public void dispose(){
        world.dispose();
    }
}
