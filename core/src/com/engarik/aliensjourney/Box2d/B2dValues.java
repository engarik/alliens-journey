package com.engarik.aliensjourney.Box2d;

public class B2dValues {
    public static final float PPM = 128;
    public static final float TIME_STEP = 1/60f;
    public static final int VEL_ITERATIONS = 8;
    public static final int POS_ITERATIONS = 3;

    // Category bits
    public static final short BIT_GROUND = 2;
    public static final short BIT_PLAYER = 4;
    public static final short BIT_SPIKES = 8;
    public static final short BIT_COIN_BRONZE = 16;
    public static final short BIT_COIN_SILVER = 32;
    public static final short BIT_COIN_GOLD = 64;
    public static final short BIT_FINISH = 128;

}
