package com.engarik.aliensjourney.Box2d.Handlers;


import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.engarik.aliensjourney.AliensJourney;

public class MyContactListener implements ContactListener{

    @Override
    public void beginContact(Contact contact) {
        Fixture fixtureA = contact.getFixtureA();
        Fixture fixtureB = contact.getFixtureB();
        AliensJourney.getInstance().getDebugger().debug("Contact listener",
                String.format("Begin contact (%s/%s)", fixtureA.getUserData(), fixtureB.getUserData()));
    }

    @Override
    public void endContact(Contact contact) {
        Fixture fixtureA = contact.getFixtureA();
        Fixture fixtureB = contact.getFixtureB();
        AliensJourney.getInstance().getDebugger().debug("Contact listener",
                String.format("End contact (%s/%s)", fixtureA.getUserData(), fixtureB.getUserData()));

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
