package com.engarik.aliensjourney;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.engarik.aliensjourney.Screens.AboutScreen;
import com.engarik.aliensjourney.Screens.LevelsScreen;
import com.engarik.aliensjourney.Screens.LoadingScreen;
import com.engarik.aliensjourney.Screens.MenuScreen;
import com.engarik.aliensjourney.Screens.PlayScreen;
import com.engarik.aliensjourney.Screens.SettingsScreen;
import com.engarik.aliensjourney.Utils.Assets;
import com.engarik.aliensjourney.Utils.Debugger;
import com.engarik.aliensjourney.Utils.MusicPlayer;

import java.util.HashMap;

public class AliensJourney extends Game{

    private AssetManager manager;
    private Assets assets;
    private SpriteBatch batch;
    private HashMap<String, Screen> screens;
    private Debugger debugger;
    private MusicPlayer musicPlayer;


    @Override
    public void create() {
        debugger = new Debugger();
        debugger.logStart();
        manager = new AssetManager();
        batch = new SpriteBatch();
       // musicPlayer = new MusicPlayer("")
        screens = new HashMap<String, Screen>();
        screens.put("LoadingScreen", new LoadingScreen(manager, assets));
        setScreen("LoadingScreen");
    }

    public void initPostLoad() {
        assets = new Assets();
        addScreens();
        setScreen("MenuScreen");
        debugger.debugLoadingTime();
        //musicPlayer.play();
    }

    public void dispose() {
        super.dispose();
        manager.dispose();
        batch.dispose();
        screens.clear();
        //musicPlayer.dispose();
        debugger.logEnd();
    }

    public void setScreen(String name) {
        if (screens.containsKey(name)) setScreen(screens.get(name));
    }

    private void addScreens() {
        screens.put("MenuScreen", new MenuScreen(manager, assets));
        screens.put("SettingsScreen", new SettingsScreen(manager, assets));
        screens.put("AboutScreen", new AboutScreen(manager, assets));
        screens.put("LevelsScreen", new LevelsScreen(manager, assets));
        screens.put("PlayScreen", new PlayScreen(manager, assets));
    }

    public SpriteBatch getBatch() {
        return batch;
    }

    public AssetManager getManager() {
        return manager;
    }

    public Assets getAssets() {
        return assets;
    }

    public Debugger getDebugger() {
        return debugger;
    }

    public MusicPlayer getMusicPlayer() {
        return musicPlayer;
    }

    private static final AliensJourney ourInstance = new AliensJourney();
    public static AliensJourney getInstance() {
        return ourInstance;
    }
    private AliensJourney() {
    }
}
