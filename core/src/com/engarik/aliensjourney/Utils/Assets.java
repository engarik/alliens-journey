package com.engarik.aliensjourney.Utils;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.engarik.aliensjourney.AliensJourney;

import java.util.HashMap;

public class Assets {

    public HashMap<String, Skin> skins;
    private AssetManager manager;

    public Assets() {
        this.manager = AliensJourney.getInstance().getManager();
        initSkins();
    }

    private void initSkins() {
        String main = "toLoad/skins/main.json";
        skins = new HashMap<String, Skin>();
        skins.put("midStdBtn", manager.get(main, Skin.class));
        skins.put("midCheckedBtn", manager.get(main, Skin.class));
        skins.put("smallStdBtn", manager.get(main, Skin.class));
        skins.put("smallCheckedBtn", manager.get(main, Skin.class));
        skins.put("sqrStdBtn", manager.get(main, Skin.class));
    }

    public Button setPos(Button button, float x, float y) {
        button.setPosition(x, y);
        return button;
    }

    /*public BitmapFont getFont() {
        return skins.get("midStdBtn").getFont("latoLightFnt");
    }*/

    public void addListener(Button button, ClickListener listener) {
        button.addListener(listener);
    }
}
