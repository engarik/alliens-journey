package com.engarik.aliensjourney.Utils;

public class Values {

    public static final String TITLE = "Alien's Journey";
    public static final String VERSION = "0.0.3";

    public static final int WIDTH = 1920;
    public static final int HEIGHT = 1080;

    public static float FX_SOUND = 0.01f;
    public static float MISC_SOUND = 0.01f;

    public static boolean DEBUG = true;

    public static int current_lvl = 0;

}
