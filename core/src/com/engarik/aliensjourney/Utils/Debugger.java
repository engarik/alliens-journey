package com.engarik.aliensjourney.Utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

import java.text.SimpleDateFormat;
import java.util.Date;


public class Debugger {

    private FileHandle logFile;
    private long start;
    private long assetsSize;


    public Debugger() {

    }

    public void debug(String tag, Object msg) {
        if (Values.DEBUG) {
            Gdx.app.log(tag, msg.toString());
            writeLog(tag + ": " + msg.toString());
        }
    }

    public void debugLoadingTime() {
        debug("Loading time", System.currentTimeMillis() - start);
    }

    public void debugAssetsSize() {
        debug("Assets size", assetsSize);
    }

    public void addAssetsSize(long increment) {
        assetsSize += increment;
    }

    private void getInfoAboutSystem() {
        debug("=== User system info", "==============");
        debug("OS", "\t\t\t\t" + System.getProperties().getProperty("os.name"));
        debug("OS Architecture", "\t" + System.getProperties().getProperty("os.arch"));
        debug("OS Version", "\t\t" + System.getProperties().getProperty("os.version"));
        debug("Username", "\t\t\t" + System.getProperties().getProperty("user.name"));
        debug("Available cores", "\t" + Runtime.getRuntime().availableProcessors());
        debug("=== End of user system info", "=======");
    }

    public void logStart() {
        if (Values.DEBUG) {
            logFile = Gdx.files.local("log.txt");
            debug("\nLog started", SimpleDateFormat.getDateInstance().format(new Date()) + " " + SimpleDateFormat.getTimeInstance().format(new Date()));
            start = System.currentTimeMillis();
            getInfoAboutSystem();
        }
    }

    public void logEnd() {
        debug("Log ended", SimpleDateFormat.getDateInstance().format(new Date()) + " " + SimpleDateFormat.getTimeInstance().format(new Date()));
    }

    public void debugFps(Label label) {
        if (Values.DEBUG) {
            label.setText("FPS " + Gdx.graphics.getFramesPerSecond());
        }
    }

    private void writeLog(String log) {
        logFile.writeString(log, true);
        logFile.writeString("\n", true);
    }

}
