package com.engarik.aliensjourney.Utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.engarik.aliensjourney.AliensJourney;

public class MusicPlayer {

    private Music music;

    public MusicPlayer(String path){
        music = Gdx.audio.newMusic(Gdx.files.internal(path));
        music.setVolume(Values.MISC_SOUND);
        music.setLooping(true);
    }

    public void play(){
        music.play();
    }

    public void setVolume(float volume){
        Values.MISC_SOUND = volume;
        music.setVolume(Values.MISC_SOUND);
    }

    public void pause(){
        music.pause();
    }

    public void stop(){
        music.stop();
    }

    public void dispose(){
        music.dispose();
    }

    public void switchMisc(){
        if(music.getVolume() != 0) {
            music.setVolume(0f);
            AliensJourney.getInstance().getDebugger().debug("Volume changed", "now - 0");
        }
        else {
            music.setVolume(0.01f);
            AliensJourney.getInstance().getDebugger().debug("Volume changed", "now - 0.01");
        }
    }
}
