package com.engarik.aliensjourney.Screens;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.engarik.aliensjourney.Handlers.ButtonHandler;
import com.engarik.aliensjourney.Handlers.ScreensHandler;
import com.engarik.aliensjourney.Utils.Assets;

public class SettingsScreen extends StdScreen{
    public SettingsScreen(AssetManager manager, Assets assets) {
        super(manager, assets);

        Image bg = new Image(manager.get("toLoad/bg/settings_bg.png", Texture.class));
        ui.addActor(bg);

        TextButton button = new TextButton("Ru", assets.skins.get("smallCheckedBtn"), "smallCheckedBtn");
        assets.addListener(button, new ButtonHandler());
        ui.addActor(assets.setPos(button, 1374, 540));

        button = new TextButton("En", assets.skins.get("smallCheckedBtn"), "smallCheckedBtn");
        assets.addListener(button, new ButtonHandler());
        ui.addActor(assets.setPos(button, 1606, 540));

        button = new TextButton("Sound", assets.skins.get("midCheckedBtn"), "midCheckedBtn");
        assets.addListener(button, new ButtonHandler());
        ui.addActor(assets.setPos(button, 1374, 359));

        button = new TextButton("Back", assets.skins.get("smallStdBtn"), "smallStdBtn");
        assets.addListener(button, new ScreensHandler("MenuScreen"));
        ui.addActor(assets.setPos(button, 64, 64));

    }
}
