package com.engarik.aliensjourney.Screens;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.engarik.aliensjourney.Handlers.ButtonHandler;
import com.engarik.aliensjourney.Handlers.ScreensHandler;
import com.engarik.aliensjourney.Utils.Assets;

public class AboutScreen extends StdScreen{

    public AboutScreen(AssetManager manager, Assets assets) {
        super(manager, assets);

        Image bg = new Image(manager.get("toLoad/bg/about_bg.png", Texture.class));
        ui.addActor(bg);

        TextButton button = new TextButton("Vk", assets.skins.get("smallStdBtn"), "smallStdBtn");
        assets.addListener(button, new ButtonHandler());
        ui.addActor(assets.setPos(button, 1374, 540));

        button = new TextButton("Rate us!", assets.skins.get("midStdBtn"), "midStdBtn");
        assets.addListener(button, new ButtonHandler());
        ui.addActor(assets.setPos(button, 1374, 359));

        button = new TextButton("Back", assets.skins.get("smallStdBtn"), "smallStdBtn");
        assets.addListener(button, new ScreensHandler("MenuScreen"));
        ui.addActor(assets.setPos(button, 64, 64));

        /*

        Button mail = new Button(assets.skins.get("mail"), "mailBtn");
        assets.addListener(mail, new ButtonHandler());
        ui.addActor(assets.setPos(mail, 1606, 540));

        button = new TextButton("Sign in:", assets.skins.get("signIn"), "signIn");
        assets.addListener(button, new ButtonHandler());
        ui.addActor(assets.setPos(button, 1374, 187));

        */

    }
}
