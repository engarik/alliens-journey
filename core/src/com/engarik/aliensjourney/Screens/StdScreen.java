package com.engarik.aliensjourney.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.engarik.aliensjourney.AliensJourney;
import com.engarik.aliensjourney.Utils.Assets;
import com.engarik.aliensjourney.Utils.Values;

public class StdScreen implements Screen{

    private FitViewport viewport;
    private FitViewport uiViewport;

    InputMultiplexer multiplexer;

    AssetManager manager;

    Assets assets;
    Stage stage;
    Stage ui;

    OrthographicCamera camera;
    OrthographicCamera uiCamera;

    Label fps;



    StdScreen(AssetManager manager, Assets assets) {
        camera = new OrthographicCamera(Values.WIDTH, Values.HEIGHT);
        camera.setToOrtho(false);

        uiCamera = new OrthographicCamera(Values.WIDTH, Values.HEIGHT);
        uiCamera.setToOrtho(false);

        viewport = new FitViewport(Values.WIDTH, Values.HEIGHT, camera);
        uiViewport = new FitViewport(Values.WIDTH, Values.HEIGHT, uiCamera);

        camera.position.set(viewport.getWorldWidth() / 2, viewport.getWorldHeight() / 2, 0);
        uiCamera.position.set(uiViewport.getWorldWidth() / 2, uiViewport.getWorldHeight() / 2, 0);

        stage = new Stage(viewport, AliensJourney.getInstance().getBatch());
        ui = new Stage(uiViewport, AliensJourney.getInstance().getBatch());

        this.manager = manager;
        this.assets = assets;

        Label.LabelStyle style = new Label.LabelStyle();
        style.font = new BitmapFont();
        style.fontColor = Color.BLACK;
        fps = new Label("", style);
        fps.setPosition(1700, 1040);
        fps.setFontScale(1.5f, 1.5f);

    }

    @Override
    public void show() {
        multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(stage);
        multiplexer.addProcessor(ui);
        Gdx.input.setInputProcessor(multiplexer);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        update(delta);
        draw();
        fps.setText("Ver. " + Values.VERSION + " FPS: " + Gdx.graphics.getFramesPerSecond());
        AliensJourney.getInstance().getBatch().begin();
        fps.draw(AliensJourney.getInstance().getBatch(), 1.0f);
        AliensJourney.getInstance().getBatch().end();

    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        uiViewport.update(width, height);
    }

    @Override
    public void pause() {}

    @Override
    public void resume() {}

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void dispose() {
        multiplexer.clear();
        manager.dispose();
        stage.dispose();
        ui.dispose();
    }

    protected void update(float delta) {
        stage.act(delta);
        ui.act(delta);
        camera.update();
        uiCamera.update();
    }

    protected void draw() {
        stage.draw();
        ui.draw();
    }
}
