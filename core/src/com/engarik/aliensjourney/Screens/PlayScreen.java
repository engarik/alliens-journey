package com.engarik.aliensjourney.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.engarik.aliensjourney.Box2d.GameWorld;
import com.engarik.aliensjourney.Handlers.ScreensHandler;
import com.engarik.aliensjourney.Utils.Assets;
import com.engarik.aliensjourney.Utils.Values;

public class PlayScreen extends StdScreen{


    private GameWorld world;


    private Image bg;


    public PlayScreen(AssetManager manager, Assets assets) {
        super(manager, assets);

        TextButton button = new TextButton("Back", assets.skins.get("smallStdBtn"), "smallStdBtn");
        assets.addListener(button, new ScreensHandler("LevelsScreen"));
        ui.addActor(assets.setPos(button, 25, 960));


        Image bg = new Image(manager.get("toLoad/bg/parallax_bg.png", Texture.class));

        world = new GameWorld();

        stage.addActor(bg);

    }


    @Override
    public void render(float delta) {
        super.render(delta);
        world.render();
    }

    @Override
    public void show() {
        super.show();
        world = new GameWorld();
    }

    public void update(float delta) {
        super.update(delta);

        handleInput(delta);

        world.update(camera);
    }

    private void handleInput(float delta) {
        world.handleInput(camera, delta);
        world.trace(camera, delta);
        if(Gdx.input.isKeyPressed(Input.Keys.NUM_1)){
            Values.current_lvl = 0;
            world = new GameWorld();
        }
        if(Gdx.input.isKeyPressed(Input.Keys.NUM_2)){
            Values.current_lvl = 1;
            world = new GameWorld();
        }
    }

    public void dispose(){
        super.dispose();
        world.dispose();
    }
}
