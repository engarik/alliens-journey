package com.engarik.aliensjourney.Screens;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.engarik.aliensjourney.Handlers.ScreensHandler;
import com.engarik.aliensjourney.Utils.Assets;

public class MenuScreen extends StdScreen{

    public MenuScreen(AssetManager manager, Assets assets) {
        super(manager, assets);

        Image bg = new Image(manager.get("toLoad/bg/menu_bg.png", Texture.class));
        ui.addActor(bg);

        TextButton button = new TextButton("Play", assets.skins.get("midStdBtn"), "midStdBtn");
        assets.addListener(button, new ScreensHandler("LevelsScreen"));
        ui.addActor(assets.setPos(button, 1336, 673));

        button = new TextButton("Settings", assets.skins.get("midStdBtn"), "midStdBtn");
        assets.addListener(button, new ScreensHandler("SettingsScreen"));
        ui.addActor(assets.setPos(button, 1336, 497));

        button = new TextButton("About", assets.skins.get("midStdBtn"), "midStdBtn");
        assets.addListener(button, new ScreensHandler("AboutScreen"));
        ui.addActor(assets.setPos(button, 1336, 321));

    }
}
