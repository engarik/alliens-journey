package com.engarik.aliensjourney.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.MusicLoader;
import com.badlogic.gdx.assets.loaders.SkinLoader;
import com.badlogic.gdx.assets.loaders.SoundLoader;
import com.badlogic.gdx.assets.loaders.TextureLoader;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.engarik.aliensjourney.AliensJourney;
import com.engarik.aliensjourney.Utils.Assets;

public class LoadingScreen extends StdScreen {

    private ProgressBar bar;

    public LoadingScreen(AssetManager manager, Assets assets) {
        super(manager, assets);
        Load();
        AliensJourney.getInstance().getDebugger().debugAssetsSize();
        setUi();
    }

    public void render(float delta) {
        super.render(delta);
        if (manager.update()) {
            AliensJourney.getInstance().initPostLoad();
        }
        bar.setValue(manager.getProgress());
    }

    private void Load() {
        setLoaders();
        loadAssets("toLoad/", "png", Texture.class);
        loadAssets("toLoad/levels/", "tmx", TiledMap.class);
        loadAssets("toLoad/sounds/", "ogg", Sound.class);
        loadAssets("toLoad/sounds/", "mp3", Music.class);
        loadAssets("toLoad/", "json", Skin.class);
    }

    private void loadAssets(String path, String extension, Class type) {
        FileHandle folder = Gdx.files.internal(path);
        if (folder.isDirectory())
            for (FileHandle file : folder.list())
                loadAssets(file.path(), extension, type);
        if (folder.extension().equals(extension)) {
            manager.load(folder.path(), type);
            AliensJourney.getInstance().getDebugger().debug("Asset loaded", folder.path());
            AliensJourney.getInstance().getDebugger().addAssetsSize(folder.length());
        }
    }

    private void setLoaders() {
        InternalFileHandleResolver resolver = new InternalFileHandleResolver();
        manager.setLoader(TiledMap.class, new TmxMapLoader(resolver));
        manager.setLoader(Music.class, new MusicLoader(resolver));
        manager.setLoader(Sound.class, new SoundLoader(resolver));
        manager.setLoader(Texture.class, new TextureLoader(resolver));
        manager.setLoader(Skin.class, new SkinLoader(resolver));
    }

    private void setUi() {
        Image bg = new Image(new Texture(Gdx.files.internal("toLoad/bg/loading_bg.png")));
        ui.addActor(bg);
        ProgressBar.ProgressBarStyle style = new ProgressBar.ProgressBarStyle(null, new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("toLoad/sprites/knob.png")))));
        bar = new ProgressBar(0.01f, 1f, 0.02f, false, style);
        bar.setPosition(466, 268);
        bar.setSize(988, 242);
        ui.addActor(bar);
    }
}
