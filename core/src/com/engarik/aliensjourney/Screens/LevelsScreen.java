package com.engarik.aliensjourney.Screens;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.engarik.aliensjourney.Handlers.LevelHandler;
import com.engarik.aliensjourney.Handlers.ScreensHandler;
import com.engarik.aliensjourney.Utils.Assets;

public class LevelsScreen extends StdScreen {
    public LevelsScreen(AssetManager manager, Assets assets) {
        super(manager, assets);

        Image bg = new Image(manager.get("toLoad/bg/levelSelect_bg.png", Texture.class));
        ui.addActor(bg);

        TextButton textButton = new TextButton("Back", assets.skins.get("smallStdBtn"), "smallStdBtn");
        assets.addListener(textButton, new ScreensHandler("MenuScreen"));
        ui.addActor(assets.setPos(textButton, 64, 64));


        textButton = new TextButton("1", assets.skins.get("smallStdBtn"), "smallStdBtn");
        assets.addListener(textButton, new LevelHandler(0));
        ui.addActor(assets.setPos(textButton, 1336, 673));

        textButton = new TextButton("2", assets.skins.get("smallStdBtn"), "smallStdBtn");
        assets.addListener(textButton, new LevelHandler(1));
        ui.addActor(assets.setPos(textButton, 1600, 673));


    }

}
